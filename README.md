# EchoSounderAI

Fish species identification from the splitbeam echosounder (Simrad WBT Mini) data

### Sensor data info
Attaching some data from the drone – it does have a conductivity sensor but of course it measures only surface conductivity (at about 30 cm depth). It does also have some other sensors that could be useful for interpreting some of the echosounder data, i.e.

Water temperature (CT Temp)
Conductivity as said above, called CT Cond)
Turbidity (ECO3_Beta)
Algae (Chlorophyll fluorescence, ECO3_Chl)
Cyanobacteria (ECO3_PC)
 

All those data are in the attached textfile, measured at 10 min intervals.



### Interesting research papers:
1) Species identification of pelagic fish schools on the South African continental shelf using acoustic descriptors [link](https://academic.oup.com/icesjms/article/58/1/275/603529)
2) Practical implementation of real-time fish classification from acoustic broadband echo sounder data - RealFishEcho [link](https://www.wur.nl/en/Publication-details.htm?publicationId=publication-way-353333383235)
3) A Deep Learning-based Framework for the Detection of Schools of Herring in Echograms [link](https://arxiv.org/pdf/1910.08215.pdf)


### Ideas:
-  