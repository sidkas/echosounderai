import os, sys, pathlib, random, errno
from tqdm import tqdm
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from PIL import Image
from sklearn.neighbors import NearestNeighbors

project_dir = pathlib.Path(__file__).resolve().parents[0]

def mkdir_p(path, folder_name = None):
    if folder_name:
        abs_folder_path = os.path.join(path, folder_name)
    else:
        abs_folder_path = path
    try:
        os.makedirs(abs_folder_path)
        
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(abs_folder_path):
            pass
        else:
            raise
    return abs_folder_path  

def find_all_files_in_folder(folder, extension):
    if os.path.exists(folder):
        paths= []
        check_paths = []
        for path, subdirs, files in os.walk(folder):
            for file_path in files:
                if file_path.endswith(extension) and not file_path.startswith('.'):
                    if not any(file_path in s for s in check_paths):
                        paths.append(os.path.join(path , file_path))
                        check_paths.append(file_path)
        return paths
    else:
        raise Exception("path does not exist -> "+ folder)

def load_image(img_path):
    img = Image.open(img_path)
    gray = img.convert('L')

    bitmap = np.array(gray)

    return bitmap

def region_growing_segmentation(bitmap:np.ndarray,max_radius=5):

    x,y = np.where(bitmap>2)

    pixel_positions_list = [[xs,ys] for xs,ys in zip(x,y)]
    pixel_positions = np.array(pixel_positions_list)

    nbrs = NearestNeighbors(n_neighbors = 20, radius=2)
    nbrs.fit(pixel_positions)
    distances, indexes = nbrs.kneighbors(pixel_positions)


    seeds_size = int(len(pixel_positions)*0.1)
    seed_indexes = random.sample(list(np.arange(0,len(pixel_positions))),seeds_size)

    regions_indexes = []
    assigned_points = []
    for ls in list(seed_indexes):
        S = set()
        S.add(ls)
        R = []
        while len(S)>0:
            p = S.pop()
            for d,ix in zip(distances[p],indexes[p]):
                if d<=max_radius and ix not in assigned_points:
                    S.add(ix)
                    R.append(ix)
                    assigned_points.append(ix)
        if len(R)>0:
            regions_indexes.append(R)



    regions = [pixel_positions[region] for region in regions_indexes]

    return regions
from scipy.spatial import ConvexHull, convex_hull_plot_2d

def plot_regions(bitmap:np.ndarray,regions, points_per_region=10, file_path=None):
    plt.figure()
    plt.imshow(bitmap,cmap="gray")
    for points in regions:
        if len(points)>points_per_region:
            hull = ConvexHull(points)
            # plt.plot(points[:,1], points[:,0], 'o')
            for simplex in hull.simplices:
                plt.plot(points[simplex, 1], points[simplex, 0], 'y-')
            plt.plot(points[hull.vertices,1], points[hull.vertices,0], 'y')
            plt.plot(points[hull.vertices[0],1], points[hull.vertices[0],0], 'w')
    
    if file_path is None:
        plt.show()
    else:
        plt.savefig(file_path)


def test_region_growing_segmentaion():
    bitmap = load_image("data/AI_annotation_images/School_detect3.BMP")

    segmented_regions = region_growing_segmentation(bitmap,max_radius=5)

    plot_regions(bitmap,segmented_regions,points_per_region=15)

def run_region_growing_segmentation(dataset_path):
    bitmap_paths = find_all_files_in_folder(dataset_path,".BMP")

    results_path = os.path.join(project_dir,"results")

    mkdir_p(results_path)

    for bitmap_path in tqdm(bitmap_paths):
        bitmap = load_image(bitmap_path)

        segmented_regions = region_growing_segmentation(bitmap,max_radius=5)
        save_plot_path = os.path.join(results_path, os.path.split(bitmap_path)[1].replace('.BMP', ".png"))
        plot_regions(bitmap,segmented_regions,points_per_region=15,file_path=save_plot_path)


if __name__=='__main__':
    # test_region_growing_segmentaion()
    run_region_growing_segmentation("data/AI_annotation_images")


    

