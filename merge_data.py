import os, sys, pathlib
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from tqdm import tqdm

project_dir = pathlib.Path(__file__).resolve().parents[0]



def find_files_in_folder(folder, extension):
    if os.path.exists(folder):
        paths= []
        for file in os.listdir(folder):
            if file.endswith(extension):
                paths.append(os.path.join(folder , file))

        return paths
    else:
        raise Exception("path does not exist -> "+ folder)


def preprocess_echosounder_csv(csv_path):
    df = pd.read_csv(csv_path,low_memory=False)
    df = df.set_index(df.columns[0])
    df_t = df.T
    df_t = df_t.drop(["Variable_index", "Variable_name"])
    df_t["Time"] = df_t[["Ping_date","Ping_time"]].agg(' '.join, axis=1)
    df_t["datetime"] = pd.to_datetime(df_t['Time'])
    df_t = df_t.drop(["Ping_date","Ping_time",'Time'], axis=1)
    return df_t

def preprocess_sensordata():
    sensor_data_path = os.path.join(project_dir,"data","raw","SB1810D2.txt")
    sensor_df = pd.read_csv(sensor_data_path,low_memory=False,sep="\t")
    sensor_df['datetime'] = pd.to_datetime(sensor_df['Time'])
    sensor_df = sensor_df[["datetime",'Lat','Long','V','Temperature', 'CTTemp', 'CTCond','ECO3_Beta', 'ECO3_CHL', 'ECO3_PC']]
    return sensor_df



def main():
    csv_files = find_files_in_folder(os.path.join(project_dir,"data","raw","WidebandFreqRespAI"),".csv")

    sensor_df = preprocess_sensordata()
    combined_df = pd.DataFrame()
    for csv_path in tqdm(csv_files):
        echosounder_df = preprocess_echosounder_csv(csv_path)
        merge_df = pd.merge_asof(echosounder_df,sensor_df,on='datetime')
        combined_df = pd.concat([combined_df,merge_df],ignore_index=True)

    combined_df.to_csv(os.path.join(project_dir,"data","interim",'combined_data.gz'), compression='gzip', index=False)

if __name__=="__main__":
    main()